let intervalId;
let stimems = new Date().getTime();
chrome.storage.local.set({msStart :stimems});
let startButton = document.getElementById('starttimebutton');
let finishButton = document.getElementById('finishtimebutton');
let resetbutton = document.getElementById('reset');
let differnece = document.getElementById('difference');
if (startButton && finishButton && differnece && resetbutton) {
    startButton.addEventListener('click',
        function (){
            let seconds = Math.floor((stimems / 1000) % 60);
            let minutes = Math.floor((stimems / (1000 * 60)) % 60);
            let hours = Math.floor(((stimems / (1000 * 60 * 60)) % 24) + 7);
            hours = hours < 10 ? '0' + hours : hours;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;
            document.getElementById('starttime').textContent = hours + " : " + minutes + " : " + seconds;
            var sArray = { hours : hours, minutes: minutes, seconds : seconds};
        }, {once: true});
    finishButton.addEventListener('click',
        function (){
            let currentime = new Date().getTime();
            let seconds = Math.floor((currentime / 1000) % 60);
            let minutes = Math.floor(((currentime / 1000) / 60) % 60);
            let hours = Math.floor(((currentime / (1000 * 60 * 60)) % 24) + 7);
            hours = hours < 10 ? '0' + hours : hours;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;
            var entime = {hours , minutes, seconds};
            document.getElementById('endtime').textContent = hours + " : " + minutes + " : "  + seconds;
        })
    startButton.addEventListener('click',
        function (){
            intervalId = setInterval(function (){
                let currentime = new Date().getTime();
                let diff = currentime - stimems;
                let seconds = Math.floor((diff / 1000) % 60);
                let minutes = Math.floor(((diff / 1000) / 60) % 60);
                let hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
                let days = Math.floor(diff / (1000 * 60 * 60 * 24));
                days = days < 10 ? '0' + days : days;
                hours = hours < 10 ? '0' + hours : hours;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                seconds = seconds < 10 ? '0' + seconds : seconds;
                differnece.textContent = days + " : " + hours + " : " + minutes + " : " + seconds;
            },1000);
        })
    finishButton.addEventListener('click',
        function (){
            let currnetime = new Date().getTime();
            let diff = currnetime - stimems;
            let seconds = Math.floor((diff / 1000) % 60);
            let minutes = Math.floor(((diff / 1000) / 60) % 60);
            let hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
            let days = Math.floor(diff / (1000 * 60 * 60 * 24));
            days = days < 10 ? '0' + days : days;
            hours = hours < 10 ? '0' + hours : hours;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;
            differnece.textContent = days + " : " + hours + " : " + minutes + " : " + seconds;
            clearInterval(intervalId);
        })
    resetbutton.addEventListener('click',
        function (){
            chrome.storage.local.clear();
        })
}
